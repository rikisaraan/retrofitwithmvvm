package com.example.androidviewmodel;

public class Value {
    String success;
    String message;
    String status;
    String kode;

    public String getSuccess() {
        return success;
    }

    public String getMessage() {
        return message;
    }

    public String getStatus() {
        return status;
    }

    public String getKode() {
        return kode;
    }
}
