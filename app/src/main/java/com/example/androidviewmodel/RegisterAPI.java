package com.example.androidviewmodel;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface RegisterAPI {
    @FormUrlEncoded
    @POST("InsertExample")
    Call<Value> daftar(@Field("nama_role") String nama_role);
}
