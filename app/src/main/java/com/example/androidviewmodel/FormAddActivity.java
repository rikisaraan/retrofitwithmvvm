package com.example.androidviewmodel;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class FormAddActivity extends AppCompatActivity {

    public static final String URL = "http://192.168.70.57/e-ticketing/api-eticketing/index.php/";
    private ProgressDialog progress;

    EditText editTextRoleName;
    Button buttonDaftar;
    String nama_role;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_add);

        editTextRoleName = findViewById(R.id.editTextRoleName);
        buttonDaftar = findViewById(R.id.buttonDaftar);

        buttonDaftar.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                daftar();
            }
        });
    }

    public void daftar(){
        progress = new ProgressDialog(this);
        progress.setCancelable(false);
        progress.setMessage("Loading ...");
        progress.show();

        //mengambil data dari edittext
        nama_role = editTextRoleName.getText().toString();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        RegisterAPI api = retrofit.create(RegisterAPI.class);
        Call<Value> call = api.daftar(nama_role);
        call.enqueue(new Callback<Value>() {
            @Override
            public void onResponse(Call<Value> call, Response<Value> response) {
                Log.e("Response",new Gson().toJson(response.body()) );
                String success = response.body().getSuccess();
                String message = response.body().getMessage();
                progress.dismiss();
                if (success.equals("1")) {
                    Toast.makeText(FormAddActivity.this, message, Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(FormAddActivity.this, message, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Value> call, Throwable t) {
                progress.dismiss();
                Toast.makeText(FormAddActivity.this, "Jaringan Error! "+t.getMessage()+""+nama_role+" - ", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
